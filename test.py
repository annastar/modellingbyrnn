'''
Created on Feb 4, 2017

@author: Kamran Binaee
'''
import pandas as pd
from astropy.io.fits.diff import RawDataDiff
TrailList = pd.read_pickle('trialType_Success.pickle') 
print(len(TrailList))

trial_types = ['t1', 't2', 't3', 't4', 't5', 't6', 't7', 't8', 't9']
types_dict_train = {}
types_dict_test = {}

total=0;

test_num=10

for curtype in trial_types:
    trialsListTest=[]
    trailsListTrain=[]
    forTest=0;
    for trial, ttype in TrailList.items():
        if ttype == curtype:
            #print(ttype)
            if forTest<10:
                trialsListTest.append(trial)
                forTest=forTest+1
            else:   
                trailsListTrain.append(trial)
    
            
    types_dict_train[curtype]=trailsListTrain
    types_dict_test[curtype]=trialsListTest
    total = total + len(trailsListTrain) + len(trialsListTest)
    print(curtype + " number test: " + str(len(trialsListTest)))
    print(curtype + " number train: " + str(len(trailsListTrain)))

print(total)

filename = 'DataSet_Formatted_Success.pickle'
rawDataSuccess = pd.read_pickle(filename)
# print(rawDataSuccess)

rawDataTest = {}
rawDataTrain = {}

for curtype in trial_types:
    trialsList = types_dict_test[curtype]
    listOfDict=[]
    for trialname in trialsList:
        valueDict={trialname:rawDataSuccess[trialname]}
        listOfDict.append(valueDict)
        
    rawDataTest[curtype]=listOfDict
    
    trialsList = types_dict_train[curtype]
    listOfDict=[]
    for trialname in trialsList:
        valueDict={trialname:rawDataSuccess[trialname]}
        listOfDict.append(valueDict)
        
    rawDataTrain[curtype]=listOfDict


pd.to_pickle(rawDataTest, '.\Datasets\Success\DataSet_Success_10_trials_test.pickle')
pd.to_pickle(rawDataTrain, '.\Datasets\Success\DataSet_Success_num-10_trials_train.pickle')

# for type in trial_types:
#     for trail in TrailList.keys():
#         if TrailList[trail] is type:
#             types_dict = {type:trail}
# print(types_dict)

