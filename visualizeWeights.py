'''
Created on Mar 21, 2017

@author: Kamran Binaee
'''

import matplotlib.pyplot as plt
import numpy as np
import os
import time
import csv
import sys
from sklearn.metrics import mean_squared_error
import math
from keras.callbacks import  EarlyStopping
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.optimizers import SGD,RMSprop,Adam
from keras import backend as K
from keras.callbacks import ModelCheckpoint
import pandas as pd
from keras.engine.topology import Layer
from matplotlib.pyplot import axis
from trainTheModel import createRNN


if __name__=="__main__":
    windowSize = [15, 20, 25, 30, 35, 40, 45]#, 50, 55, 60, 65, 70, 75] #, 80]
    #windowSize = [15]
    weights_path = './Results/Weights/25_Input_LSTM_50/weights_'
    #weights_path = './Results/Weights/weights_'
    imageList = []
    model = []
    for inputSeqLength in windowSize:
        model = createRNN(inputSeqLength, None, None)
        model.load_weights(weights_path + str(inputSeqLength) + '_0.0001_150')
        layerDict = dict([(layer.name,layer) for layer in model.layers])
        #myKeys layerDict.keys():
#         print(layerDict.keys())
#         print(len(layerDict['lstm_1'].get_weights()))
#         for i in range((len(layerDict['lstm_1'].get_weights()))):
#             print('\n size = ', layerDict['lstm_1'].get_weights()[i].size)
#         for myKeys in layerDict.keys():
#             print(layerDict[myKeys])
        for e in zip(model.layers[0].trainable_weights, model.layers[0].get_weights()):
            print('Param %s:\n     Size = (%s)' % (e[0], e[1].shape))
            if(e[1].shape == (25,50)):
                imageList.append(e[1])
                currentImage =  abs(e[1])
                #print('Done!')
                fig = plt.figure(figsize=(10,10))
                ax = fig.add_subplot(1,1,1)
                # major ticks every 20, minor ticks every 5                                      
                major_ticks = np.arange(0, 50, 5)                                              
                minor_ticks = np.arange(0, 50, 1)                                               
                
                ax.set_xticks(major_ticks)                                                       
                ax.set_xticks(minor_ticks, minor=True)                                           
                ax.set_yticks(major_ticks)                                                       
                ax.set_yticks(minor_ticks, minor=True)                                           
                
                # and a corresponding grid                                                       
                
                ax.grid(which='both')                                                            
                
                # or if you want differnet settings for the grids:                               
                ax.grid(which='minor', alpha=0.5)                                                
                ax.grid(which='major', alpha=0.9)
                plt.imshow(currentImage, cmap="hot")
                plt.xlabel('LSTM Units')
                plt.ylabel('Feature')
                plt.colorbar()
                plt.savefig(str(e[0]) + '_' + str(inputSeqLength) + '_Weights.png')


            #print('Param %s:\n%s' % (e[0],e[1]))
            #print(layer)
            
            #print(layerDict[myKeys])
    
    