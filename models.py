'''
Created on Feb 4, 2017

@author: Kamran Binaee
'''
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.optimizers import SGD, Adam, RMSprop
from keras import backend as K
from keras.callbacks import ModelCheckpoint


def LSTM_50_50_50_1prediction(input_seq_length, optimizer = 'adam', weights_path = None):
    """LSTM model that has 3 LSTM layers with 50 units each and one Dense layer on top.
    Model can predict one time step vector with 5 values;
    The model is tuned for optimizer: optimizer = Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    """
    model = Sequential()
    layers = [10, 50, 50, 5]
    # first layer
    model.add(LSTM(
            input_dim=layers[0],
            output_dim=layers[1],input_length =input_seq_length,
            return_sequences=True, unroll=True))
    model.add(Dropout(0.2))
    # second layer
    model.add(LSTM(
           layers[2],
           return_sequences=True, unroll=True))
    model.add(Dropout(0.2))
    # third layer
    model.add(LSTM(
           layers[2],
           return_sequences=False, unroll=True))
    model.add(Dropout(0.2))
    # DEnce layer
    model.add(Dense(
            output_dim=layers[3]))
    model.add(Activation("linear"))
    
    optimizer = Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    #rmsprop = RMSprop(lr=0.001, rho=0.9, epsilon=1e-08, decay=0.0)
    model.compile(loss="mse", optimizer=optimizer)
    print('model compiled...')
    if weights_path:
        model.load_weights(weights_path)
    return model

