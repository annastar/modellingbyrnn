import matplotlib.pyplot as plt
import numpy as np
import os
import time
import csv
from sklearn.metrics import mean_squared_error
import math
from keras.callbacks import  EarlyStopping
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.optimizers import SGD,RMSprop,Adam
from keras import backend as K
K.set_image_dim_ordering('th')
# #fixing bug of TensorFlow for DropOut:  'module' object has no attribute 'control_flow_ops'
#import tensorflow as tf
#tf.python.control_flow_ops = tf


np.random.seed(1234)

#cod for unpicling dataset batch
def unpickle(file):
    import pickle
    fo = open(file, 'rb')
    dict = pickle.load(fo,  encoding='latin1') 
    fo.close()
    return dict

def data_power_consumption(path_to_dataset, sequence_length):

    result = []
    result_mean = []
    result_std = []
    
    fname_without_picle = path_to_dataset[:-6]
    nb_input_seq = path_to_dataset[-2]
    data=unpickle(path_to_dataset)
    #calculate mean and variance calculation
    for feature in range(data.shape[0]):
        feat = data [feature,:,:]
        feat_mean = feat.mean()
        result_mean.append(feat_mean)
        feat_std = feat.std()
        result_std.append(feat_std) 
    
    for sample in range(data.shape[2]):
    #take each data sample
        seq =[]
        for sequen in range(data.shape[1]):
        #create a matrix [measurement in a sequence, features] for each data sample
            seq.append(data[:,sequen,sample]) # seq shape [5 sequence; 10 features]
        result.append(seq)
    result = np.array(result)  # shape (2049230, 5,10)
    
    for index in range(len(result_mean)):
        result[:,:,index] -= result_mean[index]
        result[:,:,index] = result[:,:,index]/result_std[index]
        
    print ("Shift : ", result_mean)
    print ("STD : ", result_std)  
    print ("Data  : ", result.shape)
     
    row = round(0.9 * result.shape[0])
    train = result[:row, :]
    #np.random.shuffle(train)
    X_train = train[:, :-1,:]
    y_train = train[:, -1,0:5]
    X_test = result[row:, :-1,:]
    y_test = result[row:, -1,0:5]
    
    
    
    return [X_train, y_train, X_test, y_test]

def SaveValuesIntoNpArray(x,fname):
    """Save matrix of image into numpy array. x - image matrix, fname - path/name of future file without extension"""
    fname = fname +'.npy'
    np.save(fname, x, allow_pickle=True, fix_imports=True)

def PlotLossHist():
    ###plot Loss function function
    #code for reading file line by line
    fname = 'loss_history_train_RNN'
    with open(fname) as f:
        loss_train = f.readlines()
    f.close()
    fname = 'loss_history_evaluate_RNN'
    with open(fname) as f:
        loss_eval = f.readlines()
    f.close()
    # plot history for loss
    plt.figure()
    plt.plot(loss_train)
    plt.plot(loss_eval)
    plt.title('model loss 50, 100 layers')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train','eval'], loc='upper left')
    plt.grid(True)
    

def build_model(seq_length):

    model = Sequential()
    layers = [10, 50, 50, 5]
    
    model.add(LSTM(
            input_dim=layers[0],
            output_dim=layers[1],input_length = seq_length,
            return_sequences=True, unroll=True))
    model.add(Dropout(0.2))
    
    model.add(LSTM(
           layers[2],
           return_sequences=True, unroll=True))
    model.add(Dropout(0.2))
    model.add(LSTM(
           layers[2],
           return_sequences=False, unroll=True))
    model.add(Dropout(0.2))
    model.add(Dense(
            output_dim=layers[3]))
    model.add(Activation("linear"))
    start = time.time()
     
    adam = Adam(lr=0.0005, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0001)
    rmsprop =RMSprop(lr=0.001, rho=0.9, epsilon=1e-08, decay=0.0)
    model.compile(loss="mse", optimizer=adam)
    print ("Compilation Time : ", time.time() - start)
    return model


def run_network(model=None, data=None):
    
    
    for fname in os.listdir('./Kamran_dataset/Varying_input_sequence/'):
        
        path_to_dataset = './Kamran_dataset/Varying_input_sequence/'+fname
        fname_without_picle = fname[:-7]
        nb_input_seq = fname_without_picle[-2:]
        length_insequence = int(nb_input_seq)
        print("length of sequence {}, file name {}".format(length_insequence,fname))
       
        if data is None:
            print ('Loading data... ')
            X_train, y_train, X_test, y_test = data_power_consumption(path_to_dataset,length_insequence)
            
        else:
            X_train, y_train, X_test, y_test = data
    
        print ('\nData Loaded. Compiling...\n')
        model = None
        if model is None:
            model = build_model(length_insequence-1)
        try:
            cb = EarlyStopping(monitor='val_loss', patience=7, verbose=0, mode='auto')   
            history = model.fit(X_train, y_train,batch_size=128, nb_epoch=80, callbacks = [cb], validation_split=0.15, shuffle=True)
                
            
            fname_weights = './Old_data_varing_in_seq_50_50_50/weights_50_50_50/wieghts_rnn505050_inseq{}'.format(nb_input_seq)
            model.save_weights(fname_weights)
             
            predicted = model.predict(X_test)
            predicted = np.array(predicted)
            #predicted = np.reshape(predicted, (predicted.size,))
            print('Predicted shape', predicted.shape)
            #y_test1 = y_test[:,0,:]
            print('Test data shape',y_test.shape)
            mse_predict = mean_squared_error(predicted, y_test, multioutput='raw_values')
            print('Test Score1: MSE', mse_predict)
            
            file_dir = './Old_data_varing_in_seq_50_50_50/loss_train/'
            #os.makedirs(file_dir,exist_ok=True)
            fname_loss = './Old_data_varing_in_seq_50_50_50/loss_train/loss_history_train_RNN_inseq_{}'.format(nb_input_seq)
            f = open(fname_loss, 'w')
            for item in history.history['loss']:
                f.write('{}\n'.format(item))
            f.close()
            file_dir = './Old_data_varing_in_seq_50_50_50/loss_val/'
            #os.makedirs(file_dir,exist_ok=True)
            fname_loss = './Old_data_varing_in_seq_50_50_50/loss_val/loss_history_val_RNN_inseq{}'.format(nb_input_seq)
            f = open(fname_loss, 'w')
            for item in history.history['val_loss']:
                f.write('{}\n'.format(item))
            f.close()
        except KeyboardInterrupt:
            print ('Training duration (s) : ', time.time())
            return model, y_test, 0
        print('plot figures')
        try:
            file_dir = './Old_data_varing_in_seq_50_50_50/predicted_data/'
            #os.makedirs(file_dir,exist_ok=True)
            fname_predicted = './Old_data_varing_in_seq_50_50_50/predicted_data/predict_inseq{}'.format(nb_input_seq)
            SaveValuesIntoNpArray(predicted,fname_predicted)
            
            file_dir = './Old_data_varing_in_seq_50_50_50/test_data/'
            #os.makedirs(file_dir,exist_ok=True)
            fname_test_data = './Old_data_varing_in_seq_50_50_50/test_data/data_test_inseq{}'.format(nb_input_seq)
            SaveValuesIntoNpArray(y_test,fname_test_data)
            
            file_dir = './Old_data_varing_in_seq_50_50_50/mse_features/'
            #os.makedirs(file_dir,exist_ok=True)
            fname_mse = './Old_data_varing_in_seq_50_50_50/mse_features/mse_pred_inseq{}'.format(nb_input_seq)
            SaveValuesIntoNpArray(mse_predict,fname_mse)
        except Exception as e:
            print (str(e))
    #print ('Training duration (s) : ', time.time() - global_start_time)
#def main():
#    run_network()
#if __name__ == '__main__':
#    main()

run_network()

plt.show()