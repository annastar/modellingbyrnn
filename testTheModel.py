'''
Created on Mar 24, 2017

@author: Kamran Binaee
'''

import matplotlib.pyplot as plt
import numpy as np
import os
import time
import csv
import sys
from sklearn.metrics import mean_squared_error
import math
from keras.callbacks import  EarlyStopping
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.optimizers import SGD,RMSprop,Adam
from keras import backend as K
from keras.callbacks import ModelCheckpoint
import pandas as pd
from keras.engine.topology import Layer
from matplotlib.pyplot import axis
from blaze.compute.numpy import epoch
from trainTheModel import calculateStats
from trainTheModel import createRNN
from keras.utils.np_utils import accuracy

def PlotModelPrediction(testing_Output, modelOutput, figureName):
    """function for loss plot
    fname_Loss - full path to training loss
    fname_valLoss - full path to training loss 
    """
    handMarkerModel = '^b'
    handMarkerHuman = 'og'
    gazeMarkerModel = '^b'
    gazeMarkerHuman = 'or'
    frameRange = range(2000,4000)
    plt.figure()
    plt.plot(np.arange(len(testing_Output[frameRange,0])), testing_Output[frameRange,0], handMarkerHuman, linewidth = 4 , label = 'Subject Hand X')
    plt.plot(np.arange(len(modelOutput[frameRange,0])), modelOutput[frameRange,0], handMarkerModel, linewidth = 4 , label = 'Model Hand X')    
    plt.grid(True)
    plt.legend()
    plt.xlabel('Sample Number')
    plt.ylabel('Hand X [m]')
    plt.savefig(str(figureName)+'_Hand_X.png')

    plt.figure()
    plt.plot(np.arange(len(testing_Output[frameRange,1])), testing_Output[frameRange,1], handMarkerHuman, linewidth = 4 , label = 'Subject Hand Y')
    plt.plot(np.arange(len(modelOutput[frameRange,1])), modelOutput[frameRange,1], handMarkerModel, linewidth = 4 , label = 'Model Hand Y')    
    plt.grid(True)
    plt.legend()
    plt.xlabel('Sample Number')
    plt.ylabel('Hand Y [m]')
    plt.savefig(str(figureName)+'_Hand_Y.png')

    plt.figure()
    plt.plot(np.arange(len(testing_Output[frameRange,2])), testing_Output[frameRange,2], handMarkerHuman, linewidth = 4 , label = 'Subject Hand Z')
    plt.plot(np.arange(len(modelOutput[frameRange,2])), modelOutput[frameRange,2], handMarkerModel, linewidth = 4 , label = 'Model Hand Z')    
    plt.grid(True)
    plt.legend()
    plt.xlabel('Sample Number')
    plt.ylabel('Hand Z [m]')
    plt.savefig(str(figureName)+'_Hand_Z.png')

    plt.figure()
    plt.plot(np.arange(len(testing_Output[frameRange,7])), testing_Output[frameRange,7], gazeMarkerHuman, linewidth = 4 , label = 'Subject Gaze Azimuth')
    plt.plot(np.arange(len(modelOutput[frameRange,7])), modelOutput[frameRange,7], gazeMarkerModel, linewidth = 4 , label = 'Model Gaze Azimuth')    
    plt.grid(True)
    plt.legend()
    plt.xlabel('Sample Number')
    plt.ylabel('Gaze Azimuth [degree]')
    plt.savefig(str(figureName)+'_Gaze_X.png')

    plt.figure()
    plt.plot(np.arange(len(testing_Output[frameRange,8])), testing_Output[frameRange,8], gazeMarkerHuman, linewidth = 4 , label = 'Subject Gaze Elevation')
    plt.plot(np.arange(len(modelOutput[frameRange,8])), modelOutput[frameRange,8], gazeMarkerModel, linewidth = 4 , label = 'Model Gaze Elevation')    
    plt.grid(True)
    plt.legend()
    plt.xlabel('Sample Number')
    plt.ylabel('Gaze Elevation [degree]')
    plt.savefig(str(figureName)+'_Gaze_Y.png')

    #plt.show()
def testModel(model=None, rawDataSetFileName = None, testDataSetFileName = None, data=None, epochNumber = None):
    
    if testDataSetFileName is None:
        print('No Path Specified for Training Set!!\n ==> Quit Training\n\n')
        return None
    else:
        print('Reading Testing Data Set: ', testDataSetFileName)
        testDataSet = pd.read_pickle(testDataSetFileName)
    rawDataSet = pd.read_pickle(rawDataSetFileName)
    #print('feature 1 ', testDataSet[0,:,5])
    [mean, std] = calculateStats(rawDataSet)
    #print('\nmean = ',mean)
    #print('\nstd = ',std)
    feat_mean = mean
    feat_std = std
    #feat_mean = np.array([ -0.56496255,   1.47545376,   0.47065537, -10.3846558,   15.32644376, 38.24852388, -60.86447759,  62.19752817,  52.12331736,   0.70106737])
    #feat_std =  np.array([ 0.25888438,    0.32694775,    0.29356005,    9.35998388,   10.04664913, 60.24531354,   40.3744057,    40.61692218,  135.34311661,    0.45779025])
    
    for index in range(len(feat_mean)):
        testDataSet[:,:,index] -= feat_mean[index]
        testDataSet[:,:,index] = testDataSet[:,:,index]/(feat_std[index])
    #print('feature 1 ', testDataSet[0,:,5],'\n')
   
    #testDataSet = normalizeDataSet(testDataSet, feat_mean,  feat_std)
    #testDataSet = testDataSet - np.repeat(feat_mean, testDataSet.shape[0], testDataSet.shape[1] , axis=0)
    #testDataSet = np.divide(testDataSet, np.repeat(feat_mean, testDataSet.shape[0], testDataSet.shape[1] , axis=0))
    dataSetDimension = testDataSet.shape
    print (dataSetDimension)
    
    #print (testDataSet['t1'][0].keys())
    #print (testDataSet['t1'][0].items())
    
    #training_Input = testDataSet[:,0:dataSetDimension[1],:]
    #training_Output = testDataSet[:,dataSetDimension[1] - 1,0:16]
    testing_Input = testDataSet[:,0:dataSetDimension[1],:]
    testing_Output = testDataSet[:,dataSetDimension[1] - 1,0:16]
    
    #X_train, y_train, X_test, y_test = data
    #cb = EarlyStopping(monitor='val_loss', patience=7, verbose=0, mode='auto')  
    #history = model.fit(training_Input, training_Output,batch_size=128, nb_epoch=epochNumber, callbacks = [cb], validation_split=0.15, shuffle=True)
    
    #print('Running Model Evaluation ...')
    #score, accuracy = model.evaluate(testing_Input, testing_Output, batch_size = 128, verbose = 0 )
    #print(accuracy)
    #print(score)
    print('Running Model Prediction ...')
    modelOutput = model.predict(testing_Input, verbose=0)
    print(modelOutput.shape)
    print(testing_Output.shape)
    mse = mean_squared_error(testing_Output.T, modelOutput.T, multioutput='raw_values')
    print(mse.shape)
    print(min(mse))
    print(max(mse))
    print(np.std(mse))
    figureName = 'modelPridiction_' + str(dataSetDimension[1])
    # Un-normalizing the Data
    for index in range(modelOutput.shape[1]):
        modelOutput[:,index] = modelOutput[:,index]*(feat_std[index])
        modelOutput[:,index] += feat_mean[index]
        testing_Output[:,index] = testing_Output[:,index]*(feat_std[index])
        testing_Output[:,index] += feat_mean[index]
      
      
    PlotModelPrediction(testing_Output, modelOutput, figureName)
    
    '''
    predicted = model.predict(X_test)
    predicted = np.array(predicted)
   
    mse_predict = mean_squared_error(predicted, y_test, multioutput='raw_values')
    
    for item in history.history['loss']:
        f.write('{}\n'.format(item))

    for item in history.history['val_loss']:
        f.write('{}\n'.format(item))
    
    
    '''
    
    return model

if __name__=="__main__":
    rawDataSetFileName = r'.\Datasets\Success\DataSet_Formatted_Success.pickle'
    #dataSetFileName = None
    trialTypeFileName = r'.\Datasets\Success\trialType_Success.pickle'
    
    trainDataSetFileName = r'.\Datasets\Success\Train_DataSet_Success_6.pickle'
    testDataSetFileName = r'.\Datasets\Success\Test_DataSet_Success_6.pickle'
    trialTypeFileName = r'.\Datasets\Success\Test_TrialType_Success_6.pickle'
    trialNumberFileName = r'.\Datasets\Success\Test_TrialNumber_Success_6.pickle'
    
    trainDataSetFileName = r'.\Datasets\Success\Train_DataSet_Success_21.pickle'
    testDataSetFileName = r'.\Datasets\Success\Test_DataSet_Success_21.pickle'
    trialTypeFileName = r'.\Datasets\Success\Test_TrialType_Success_21.pickle'
    trialNumberFileName = r'.\Datasets\Success\Test_TrialNumber_Success_21.pickle'
    
    #inputSequenceList = [3, 6, 9, 12, 15, 18, 21]
    windowSize = [15, 20, 25, 30, 35, 40, 45]#, 50, 55, 60, 65, 70, 75]#, 80]
    #learningRateList = [0.0001, 0.0005]
    learningRateList = [0.0001]
    epochNumberList = [100]
        
    for epochNumber in epochNumberList:
        for learningRate in learningRateList:
            for inputSeqLength in windowSize:
                print ('\n\nTraining on Input Sequence Length = ', inputSeqLength)
                print('Learning Rate = ', learningRate)
                print('Epoch Number = ', epochNumber)
                trainDataSetFileName = r'.\Datasets\Success\Train_DataSet_Success_'+str(inputSeqLength)+'.pickle'
                testDataSetFileName = r'.\Datasets\Success\Test_DataSet_Success_'+str(inputSeqLength)+'.pickle'
                trialTypeFileName = r'.\Datasets\Success\Test_TrialType_Success_'+str(inputSeqLength)+'.pickle'
                trialNumberFileName = r'.\Datasets\Success\Test_TrialNumber_Success_'+str(inputSeqLength)+'.pickle'
                weights_path = r'.\Results\Weights\25_Input_LSTM_50\weights_' + str(inputSeqLength) + '_0.0001_100'
                model = createRNN(inputSeqLength, weights_path, learningRate)
                print('RNN-LSTM Model Created ...\n')
                model = testModel(model, rawDataSetFileName, testDataSetFileName, epochNumber = epochNumber)
            
    
    
    
