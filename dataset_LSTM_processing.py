'''
Created on Feb 4, 2017

@author: Anna Starynska
'''
import numpy as np 
import os
import pandas as pd

def unpickle(file):
    #function to read pickle file
    import pickle
    fo = open(file, 'rb')
    dict1 = pickle.load(fo,  encoding='latin1') 
    fo.close()
    return dict1

def formatDataSet(fileName, inputLength):
    rawDataSet = pd.read_pickle(fileName)
    TrailList = pd.read_pickle() 
    trainDataX = []
    trainDataY = []
    testData = []
    for trialNumber in rawDataSet.keys():
        trial = rawDataSet[trialNumber]
        if trial not in TestTrailList:
            for i in range(trial.shape[0] - inputLength):
                trainDataX.append(trial[i:i+inputLength-1])
                trainDataY.append(trial[i+inputLength])
        else:
            pass
            #create a structure for test data , shape [type of trial, trials, array [time steps,features]]
            
    
            
            
def data_process_1_ts_prediction_old_dataset(path_to_dataset):
    """ function that fragment the dataset for training and prediction
    The training batch is formated that way that each last value of samle(sequence of timesteps) 
    """
    result = []
    result_mean = []
    result_std = []
    filename = os.path.basename(path_to_dataset)
    file_root,_ = os.path.splitext(filename)
    input_sequence_length =  int(file_root[-2:-1])
    print('input sequence length', input_sequence_length)
    
    data=unpickle(path_to_dataset)
    #calculate mean and variance calculation
    for feature in range(data.shape[0]):
        feat = data [feature,:,:]
        feat_mean = feat.mean()
        result_mean.append(feat_mean)
        feat_std = feat.std()
        result_std.append(feat_std) 
    
    for sample in range(data.shape[2]):
    #take each data sample
        seq =[]
        for sequen in range(data.shape[1]):
        #create a matrix [measurement in a sequence, features] for each data sample
            seq.append(data[:,sequen,sample]) # seq shape [5 sequence; 10 features]
        result.append(seq)
    result = np.array(result)  # shape (2049230, 5,10)
    
    for index in range(len(result_mean)):
        result[:,:,index] -= result_mean[index]
        result[:,:,index] = result[:,:,index]/result_std[index]
        
    print ("Shift : ", result_mean)
    print ("STD : ", result_std)  
    print ("Data  : ", result.shape)
     
    row = round(0.9 * result.shape[0])
    train = result[:row, :]
    #np.random.shuffle(train)
    X_train = train[:, :-1,:]
    y_train = train[:, -1,0:5]
    X_test = result[row:, :-1,:]
    y_test = result[row:, -1,0:5]      
    return [X_train, y_train, X_test, y_test]
if __name__=="__main":
   TrailList = pd.read_pickle('trialType_Success.pickle') 
   print(TrailList)